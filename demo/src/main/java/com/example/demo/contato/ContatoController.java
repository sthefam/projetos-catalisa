package com.example.demo.contato;

import com.example.demo.contato.dtos.ContatoDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@RestController
@RequestMapping("/contatos")
public class ContatoController {
    @Autowired
    private ObjectMapper objectMapper;
    @PostMapping
    public Contato cadastrarContato(@RequestBody ContatoDTO contatoDTO){
        return objectMapper.convertValue(contatoDTO,Contato.class);
    }
}
