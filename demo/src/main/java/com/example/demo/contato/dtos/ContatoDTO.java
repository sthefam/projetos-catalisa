package com.example.demo.contato.dtos;

import java.time.LocalDate;

public class ContatoDTO {
    private String nome;
    private int idade;
    private LocalDate dataCadastro;

    public ContatoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }



}
