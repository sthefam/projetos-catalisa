public class Calculadora {
    public int soma(int n1, int n2) {

        if(n1 < 0 || n2 < 0){
            throw new RuntimeException("Números negativos não são válidos!");
        }

        return n1 + n2;
    }
}
