import org.junit.Assert;
import org.junit.Test;

public class CalculadoraTest {

    @Test
    public void testarMetodoSomaRetornandoResultado(){
        Calculadora calculadora = new Calculadora();

        int resultado = calculadora.soma(1,5);
        Assert.assertEquals(6,resultado);
    }

    @Test
    public void testarValidacaoDeNumeroNegativoCasoEstoureExcecao(){
        Calculadora calculadora = new Calculadora();

        Assert.assertThrows(RuntimeException.class, () -> {calculadora.soma(0,-3);});
    }

    @Test
    public void testarConverterNumeroParaAlgarismoRomano(){

        String resultado = Conversor.toRoman(2021);
        Assert.assertEquals("MMXXI",resultado);
    }

}
